from gensim.models import Word2Vec
import gensim
import multiprocessing
import logging

logging.basicConfig(
    format="%(asctime)s : %(levelname)s : %(message)s", level=logging.INFO
)

BASE_FILE_NAME = "argsme_large"
USE_SKIP_GRAM = 1
VECTOR_SIZE = 200

if USE_SKIP_GRAM == 1:
    FILE_NAME = BASE_FILE_NAME + "_" + str(VECTOR_SIZE) + "v-SG"
else:
    FILE_NAME = BASE_FILE_NAME + "_" + str(VECTOR_SIZE) + "v-BOW"

print("file: %s" % (BASE_FILE_NAME))
print("use SG: %d" % (USE_SKIP_GRAM))
print("vector size: %d" % (VECTOR_SIZE))
print("file name: %s" % (FILE_NAME))

sentences = []

print("Readining dataset %s" % (BASE_FILE_NAME))
with open(BASE_FILE_NAME + ".txt", "r") as f:
    for line in f.readlines():
        sentences.append(gensim.utils.simple_preprocess(line))

print("Starting to create w2v corpus")
cores = multiprocessing.cpu_count()

model = Word2Vec(
    sentences,
    min_count=3,
    size=VECTOR_SIZE,
    sg=USE_SKIP_GRAM,
    workers=cores - 1,
    iter=30,
)

print(len(model.wv.vocab))
print(model.wv.most_similar("laptop"))
print(model.wv.most_similar("love"))
print(model.wv.most_similar("mountain"))
print(model.wv.most_similar("city"))

model.save(FILE_NAME + "-3c.model")
