from gensim.models import Word2Vec

model_names = [
    "argsme_large_100v-BOW-3c.model",
    "argsme_large_100v-BOW.model",
    "argsme_large_100v-SG.model",
    "argsme_large_200v-BOW-3c.model",  # -> (2)
    "argsme_large_200v-BOW.model",
    "argsme_large_200v-SG-3c.model",
    "argsme_large_200v-SG.model",
    "argsme_large_300v-BOW-3c.model",
    "argsme_large_300v-BOW.model",
    "argsme_large_300v-SG.model",
    "argsme_large_400v-BOW-3c.model",  # -> gutes (1)
    "argsme_large_400v-BOW.model",
]


def print_similiar(word: str, model) -> None:
    most_similar = [x[0] for x in model.wv.most_similar(word)[:5]]

    print(f"{word}  ->\t{most_similar}")


for name in model_names:
    model = Word2Vec.load(name)
    print(f"\nTesting model {name}:")
    print(f"Number of Words: {len(model.wv.vocab)}")
    print_similiar("dog", model)
    print_similiar("laptop", model)
    print_similiar("cat", model)
    print_similiar("mountain", model)
    print_similiar("city", model)
    print_similiar("sql", model)
    print_similiar("mit", model)
    print_similiar("love", model)
    print_similiar("sex", model)
    print_similiar("canon", model)
    print_similiar("coffee", model)
    print_similiar("basketball", model)
    print_similiar("pasta", model)
    print_similiar("stanford", model)
    print_similiar("beer", model)
    print_similiar("led", model)
    print_similiar("playstation", model)
    print_similiar("stove", model)
    print_similiar("sponge", model)
    print("--------------------")
