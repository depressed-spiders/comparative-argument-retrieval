from transformers import pipeline

print("testing trained model...")
# fill_mask = pipeline(
#    "fill-mask",
#    model="./argsme-BERT",
#    tokenizer="./argsme-BERT",
# )

fill_mask = pipeline(
    "fill-mask",
    model="bert-base-uncased",
)

print(fill_mask("What is the difference between [MASK] and love?"))
print(fill_mask("What is the difference between sex and [MASK]?"))

# print(fill_mask("What is the difference between <mask> and love?"))
# print(fill_mask("What is the difference between sex and <mask> ?"))
