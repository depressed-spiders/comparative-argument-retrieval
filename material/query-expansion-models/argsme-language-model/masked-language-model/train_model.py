from tokenizers.implementations import ByteLevelBPETokenizer
from tokenizers.processors import BertProcessing

import torch

from transformers import RobertaConfig
from transformers import RobertaTokenizerFast
from transformers import RobertaForMaskedLM
from transformers import LineByLineTextDataset
from transformers import DataCollatorForLanguageModeling
from transformers import Trainer, TrainingArguments

print("loading tokenizer")
tokenizer = ByteLevelBPETokenizer("argmse-BERT/vocab.json", "argmse-BERT/merges.txt")

tokenizer._tokenizer.post_process = BertProcessing(
    ("</s>", tokenizer.token_to_id("</s>")),
    ("<s>", tokenizer.token_to_id("<s>")),
)

tokenizer.enable_truncation(max_length=512)

print("creating config...")
config = RobertaConfig(
    vocab_size=52_000,
    max_position_embeddings=514,
    num_attention_heads=12,
    num_hidden_layers=6,
    type_vocab_size=1,
)

print("loading tokenizer from files...")
tokenizer = RobertaTokenizerFast.from_pretrained("./argmse-BERT/", max_len=512)

print("create new config")
model = RobertaForMaskedLM(config=config)

print("initializing dataset from filepath with config")
dataset = LineByLineTextDataset(
    tokenizer=tokenizer,
    file_path="../argsme.txt",
    block_size=128,
)

print("creating data collator")
data_collator = DataCollatorForLanguageModeling(
    tokenizer=tokenizer, mlm=True, mlm_probability=0.15
)

print("creating training args")
training_args = TrainingArguments(
    output_dir="./argmse-BERT/",
    overwrite_output_dir=True,
    num_train_epochs=1,
    per_gpu_train_batch_size=64,
    save_steps=10_000,
    save_total_limit=2,
)

trainer = Trainer(
    model=model,
    data_collator=data_collator,
    train_dataset=dataset,
)

print("training model")
trainer.train()
print("saving model")
trainer.save_model("./argsme-BERT/")

print("testing trained model...")
from transformers import pipeline

fill_mask = pipeline(
    "fill-mask",
    model="./argsme-BERT",
    tokenizer="./argsme-BERT",
)

fill_mask = "What is the difference between sex and <mask>?"
