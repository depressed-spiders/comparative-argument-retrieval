from tokenizers import ByteLevelBPETokenizer

paths = ["../argsme.txt"]

tokenizer = ByteLevelBPETokenizer()

print("starting creating own tokenizer...")
tokenizer.train(
    files=paths,
    vocab_size=52_000,
    min_frequency=2,
    special_tokens=[
        "<s>",
        "<pad>",
        "</s>",
        "<unk>",
        "<mask>",
    ],
)

print("saving tokenizer")
tokenizer.save_model("./argsme-BERT/")
