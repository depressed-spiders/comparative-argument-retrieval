from transformers import pipeline

text_generator = pipeline(
    "text-generation",
    model="./model",
)

print("testing trained model...\n")
ret = text_generator(
    "What is the difference between sex and love?", num_return_sequences=3
)
for r in ret:
    print(r["generated_text"])

print()
ret = text_generator(
    "What is the difference between sex and love?",
    do_samples=True,
    num_return_sequences=3,
)
for r in ret:
    print(r["generated_text"])

print()
ret = text_generator(
    "What is the difference between sex and love?",
    do_samples=True,
    top_k=0,
    num_return_sequences=3,
)
for r in ret:
    print(r["generated_text"])
print()
ret = text_generator(
    "What is the difference between sex and love?",
    do_samples=True,
    top_k=0,
    top_p=0.5,
    num_return_sequences=3,
    repetition_penalty=2.0,
)
for r in ret:
    print(r["generated_text"])
